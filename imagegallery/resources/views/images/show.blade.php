@extends('layouts.app')

@section('content')

        <div class="container">
			<h3>{{ $image->title }}</h3>
			<p>{{ $image->description }}</p>
			<form action="{{ route('image-destroy', $image->id) }}" method="post">
			   @csrf
			   @method('DELETE')
			   <button type="submit" name="button" class="btn btn-danger float-right">DELETE</button>
			   <a href="{{ route('uploader-show', $image->uploader->id) }}" class="btn btn-info">
			   Go Back</a>
			   <small>Size: {{ $image->size }}</small>
			   <hr>
			   <img src="/storage/galleries/{{ $image->uploader_id }}/{{ $image->image }}" alt=
			    "{{ $image->image }}" width="990px">
			   <hr>
			</form>

		</div>	
	  
@endsection