@extends('layouts.app')


@section('content')
   <div class="container">
       <h2>Upload New Image</h2>
       <form method="post" action="{{route('image-store') }}" enctype="multipart/form-data">
       	   @csrf
       	     <input type="hidden" name="uploader-id" value="{{ $uploaderId }}">
		     <div class="form-group">
			     <label for="title">Title</label>
			     <input type="text" class="form-control" name="title" id="title"  placeholder="Enter title">
		     </div>
		     <div class="form-group">
			    <label for="description">Description</label>
			    <input type="text" class="form-control" name="description" id="description"  placeholder="Enter description">
		     </div>
		     <div class="form-group">
			    <label for="image">Image</label>
			    <input type="file" class="form-control" name="image" id="image">
		     </div>
		  
	  	    <button type="submit" class="btn btn-primary">Submit</button>
	   </form>
   </div>
@endsection