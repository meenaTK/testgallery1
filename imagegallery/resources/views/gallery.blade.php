@extends('layouts.app')

@section('content')

@if (count($uploader->images) > 0)
        <div class="container">
			<div class="row">
				@foreach ($uploader->images as $image)
					<div class="col-md-4">
						<div class="card mb-4 shadow-sm">
							<img src="/storage/galleries/{{ $uploader->id}}/{{ $image->image }}" alt=
							 "{{ $image->image}}" height="200px">
						    <div class="card-body">
								<p class="card-text">{{ $image->description }}</p>
									<div class="d-flex justify-content-between align-items-center">
										<div class="btn-group">
											<a href="{{ route('image-show', $image->id) }}" class="btn btn-sm btn-outline-secondary">View</a>
										</div>
										<small class="text-muted">{{ $image->size }}</small>
									</div>
						    </div>
						</div>
					</div>
				@endforeach	

			</div>
		</div>	
	@else
		    
		 <h3>No images yet.</h3>	

	@endif   


@endsection