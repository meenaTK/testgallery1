@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <!--<div class="card-header"></div>-->

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
            
                    Welcome User!! You are logged in!
                </div>
            </div><br><br>
        </div>
    </div>
</div>

<div class="container">
       <h2 style="color: purple"><center>Create Your new Gallery</center></h2>
       <form method="post" action="{{route('uploader-store') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" name="username" id="username"  placeholder=
                "Enter username">
            </div>
            <div class="form-group">
                <label for="galleryname">GalleryName</label>
                <input type="text" class="form-control" name="galleryname" id="galleryname"  placeholder="Enter galleryname">
            </div>
            <div class="form-group">
                <label for="cover_image">Cover Image</label>
                <input type="file" class="form-control" name="cover_image" id="cover_image">
            </div>
          
            <button type="submit" class="btn btn-primary">Submit</button>
            <a class="btn btn-primary" href="{{ url ('/uploader') }}"> View Gallery</a>
      </form>
</div>
@endsection
