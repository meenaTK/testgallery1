@extends('layouts.app')
@section('content')
<div class="container">
        @if (count($uploaders) > 0)
			<div class="row">
				@foreach ($uploaders as $uploader)
					<div class="col-md-4">
						<div class="card mb-4 shadow-sm">
							<img src="/storage/gallery_covers/{{ $uploader->cover_image}}" alt=
							 "{{ $uploader->cover_image}}" height="200px">
						    <div class="card-body">
								<p class="card-text">{{ $uploader->galleryname }}</p>
									<div class="d-flex justify-content-between align-items-center">
										<div class="btn-group">
											<a href="{{ route('uploader-show', $uploader->id) }}" class="btn btn-sm btn-outline-secondary">View</a>
                                        <!--<button type="button" class="btn btn-sm     btn-outline-secondary">Edit</button>-->
										</div>
										<small class="text-muted">{{ $uploader->username }}</small>
									</div>
						    </div>
						</div>
					</div>
				@endforeach	

			</div>
		@else
		    
		    <h3>No galleries yet.</h3>	

		@endif    
    </div>

@include('inc.messages')
@endsection
