<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Image;
use Illuminate\Support\Facades\Storage;


class ImageController extends Controller
{
     public function create(int $uploaderId)
    {
    	return view('images.create')->with('uploaderId',$uploaderId);
    }

     
     public function store(Request $request)
    {
        $this->validate ($request, [
    		'title' => 'required',
    		'description' => 'required',
    		'image' => 'required|image'
    	]);

       $filenameWithExtension = $request->file('image')->getClientOriginalName();

       $filename = pathinfo($filenameWithExtension, PATHINFO_FILENAME);

       $extension = $request->file('image')->getClientOriginalExtension();

       $filenameToStore = $filename . '_' . time() . '.' . $extension;

       $request->file('image')->storeAs('public/galleries/' .$request->input('uploader-id'), 
       $filenameToStore);

        

        $image = new Image();
        $image->title = $request->input('title');
        $image->description = $request->input('description');
        $image->image = $filenameToStore;
        $image->size = $request->file('image')->getSize();
        $image->uploader_id = $request->input('uploader-id');
        $image->save();


        return redirect('/uploader/' . $request->input('uploader-id'))->with('success','Image Uploaded successfully!');


    }


     public function show($id)
    {
        $image = Image::find($id);
        return view('images.show')->with('image', $image);
    }


    public function destroy($id)
    {
    	$image = Image::find($id);

        if(Storage::delete('public/galleries/'. $image->uploader_id.'/'.$image->image));
        {
            $image->delete();

            return redirect('/uploader')->with('success','image deleted successfully!');
        }
    }


}
