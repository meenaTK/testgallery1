<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Uploader;


class UploaderController extends Controller
{
   
   public function index()
    {
    	$uploaders=Uploader::get();
         return view('temp.coverpage')->with('uploaders',$uploaders);
        //return view('temp.index');
    }


    public function create()
    {
        return view('uploaders.create');
    }	

    public function store(Request $request)
    {
    	$this->validate ($request, [
    		'username' => 'required',
    		'galleryname' => 'required',
    		'cover_image' => 'required|image'
    	]);

        $filenameWithExtension = $request->file('cover_image')->getClientOriginalName();

        $filename = pathinfo($filenameWithExtension, PATHINFO_FILENAME);

        $extension = $request->file('cover_image')->getClientOriginalExtension();

        $filenameToStore = $filename . '_' . time() . '.' . $extension;

        $request->file('cover_image')->storeAs('public/gallery_covers', $filenameToStore);

        

        $uploader = new Uploader();
        $uploader->username = $request->input('username');
        $uploader->galleryname = $request->input('galleryname');
        $uploader->cover_image = $filenameToStore;
        $uploader->save();


        return redirect('/uploader')->with('success','Gallery created successfully!');


    }


     public function show($id)
    {
        $uploader = Uploader::with('images')->find($id);
        return view('temp.usergallery')->with('uploader', $uploader);
    }

}
