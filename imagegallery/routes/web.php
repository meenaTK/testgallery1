<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('temp.index');
});

Route::get('/gallery', function () {
    return view('gallery');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/uploader','UploaderController@index');
Route::post('/uploader/store','UploaderController@store')->name('uploader-store');
Route::get('/uploader/{id}','UploaderController@show')->name('uploader-show');



Route::get('/images/create/{uploaderId}','ImageController@create')->name('image-create');
Route::post('/images/store','ImageController@store')->name('image-store');
Route::get('/images/{id}','ImageController@show')->name('image-show');

Route::delete('/images/{id}','ImageController@destroy')->name('image-destroy');



